# Leaflet Spraycan PGIS Interface
A very simple implementation of a spraycan (airbrush) interface for Leaflet, designed for teaching purposes at the 2022 GIS Summer School at Olomouc, Czechia.

## Code:
I have broken the development down into simple steps:

1. Is a basic full-page Leaflet Map with a `click` listener to place markers onto the map at the click location
1. Incorporates mouse listeners (`mouseup`, `mousedown`, `mousemove`) to repeatedly place markers onto the map current mouse location
1. Incorporates TurfJS to add a random offset to the mouse locations
1. Replaces the default markers with small, semi-transparent circles
1. Adds the ability to clear the map *(perhaps as an 'advanced' extension for if people finish early?*

## Caveats
In order to facilitate simplicity for teaching purposes, the functionality of this is much reduced in comparison to [Map-Me](http://map-me.org), in particular:
* You cannot turn the spray on or off, so the map is effectively static
* The spray pattern is actually square, not round (though you would be unlikely to notice unless you spray the same spot for a long time)
* The paint dots and radius are both geographical, and so vary with map scale (though this might be a good thing...)
* There is no ability to turn the spraycan interface on and off (it is always on, meaning the map is not draggable)
* There are no controls permitting the reader to adjust the spray diameter, colour or dot size
* Spray patterns in the database do not relate to a given participant (just the current survey)
* There is no option for the participant to add text to the spray for context
